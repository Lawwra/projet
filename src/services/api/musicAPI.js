const apiKey = '7b5d71a6c33a0d21c7177971cf9372c5'
const baseURL = 'http://api.musixmatch.com/ws/1.1/'
const CORSAnywhere = 'https://corseverywherelibert.herokuapp.com/'

const getMusicTracksData =
    async function() {
  const response = await fetch(
      CORSAnywhere + baseURL +
      'chart.tracks.get?chart_name=top&page_size=100&apikey=' + apiKey)

  console.log('code' + response.status)
  if (response.status == 200) {
    // console.log(await response.json())
    let json = await response.json()
    return json.message.body.track_list
  }
  else {
    let json = {
      'message': {
        'header': {'status_code': 200, 'execute_time': 0.022003173828125},
        'body': {
          'artist_list': [
            {
              'artist': {
                'artist_id': 31791405,
                'artist_name': 'Jimmie Allen',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': '',
                'artist_alias_list': [],
                'artist_rating': 70,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2021-12-02T09:22:10Z',
                'begin_date_year': '',
                'begin_date': '0000-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 24410788,
                'artist_name': 'Cole Swindell',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': 'US',
                'artist_alias_list': [],
                'artist_rating': 78,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2016-08-23T14:18:07Z',
                'begin_date_year': '1983',
                'begin_date': '1983-06-30',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 13865343,
                'artist_name': 'Walker Hayes',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': 'US',
                'artist_alias_list': [],
                'artist_rating': 99,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2013-11-05T11:30:23Z',
                'begin_date_year': '1979',
                'begin_date': '1979-12-27',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 27082035,
                'artist_name': 'Morgan Wallen',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': '',
                'artist_alias_list': [],
                'artist_rating': 88,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2014-03-22T13:50:10Z',
                'begin_date_year': '',
                'begin_date': '0000-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 14016165,
                'artist_name': 'Chris Stapleton',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': 'US',
                'artist_alias_list': [
                  {
                    'artist_alias':
                        '\u30af\u30ea\u30b9\u30b9\u30c6\u30a4\u30d7\u30eb\u30c8\u30f3'
                  },
                  {'artist_alias': 'Christopher Alvin Stapleton'}
                ],
                'artist_rating': 87,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2017-10-15T11:54:10Z',
                'begin_date_year': '1978',
                'begin_date': '1978-04-15',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 141312,
                'artist_name': 'Carrie Underwood',
                'artist_name_translation_list': [{
                  'artist_name_translation': {
                    'language': 'JA',
                    'translation':
                        '\u30ad\u30e3\u30ea\u30fc\u30fb\u30a2\u30f3\u30c0\u30fc\u30a6\u30c3\u30c9'
                  }
                }],
                'artist_comment': '',
                'artist_country': 'US',
                'artist_alias_list': [
                  {
                    'artist_alias':
                        '\u30ad\u30e3\u30ea\u30fc\u30fb\u30a2\u30f3\u30c0\u30fc\u30a6\u30c3\u30c9'
                  },
                  {'artist_alias': 'Carrie M. Underwood'},
                  {'artist_alias': 'Carrie Marie Underwood'}
                ],
                'artist_rating': 75,
                'artist_twitter_url': 'https://twitter.com/carrieunderwood',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2021-07-20T20:16:53Z',
                'begin_date_year': '1983',
                'begin_date': '1983-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 24812222,
                'artist_name': 'Cody Johnson',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': '',
                'artist_alias_list': [],
                'artist_rating': 93,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2014-05-09T20:31:01Z',
                'begin_date_year': '',
                'begin_date': '0000-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 117,
                'artist_name': 'Pink Floyd',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': 'GB',
                'artist_alias_list': [
                  {
                    'artist_alias':
                        '\u30d4\u30f3\u30af \u30d5\u30ed\u30a4\u30c9'
                  },
                  {'artist_alias': 'ping ke fo luo yi de'}, {
                    'artist_alias':
                        '\u30d4\u30f3\u30af\u30fb\u30d5\u30ed\u30a4\u30c9'
                  },
                  {'artist_alias': 'Floyd'}, {'artist_alias': 'The Pink Floyd'},
                  {'artist_alias': 'Pink Floid'},
                  {'artist_alias': '\ud551\ud06c \ud50c\ub85c\uc774\ub4dc'},
                  {'artist_alias': 'Pink Floyd'}
                ],
                'artist_rating': 80,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2016-06-30T10:10:25Z',
                'begin_date_year': '1965',
                'begin_date': '1965-00-00',
                'end_date_year': '2014',
                'end_date': '2014-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 28830691,
                'artist_name': 'Jack Harlow',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': '',
                'artist_alias_list': [],
                'artist_rating': 79,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2021-10-26T09:24:11Z',
                'begin_date_year': '',
                'begin_date': '0000-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            },
            {
              'artist': {
                'artist_id': 24505463,
                'artist_name': 'Harry Styles',
                'artist_name_translation_list': [],
                'artist_comment': '',
                'artist_country': '',
                'artist_alias_list': [{
                  'artist_alias':
                      '\u30cf\u30ea\u30fc\u30b9\u30bf\u30a4\u30eb\u30ba'
                }],
                'artist_rating': 87,
                'artist_twitter_url': '',
                'artist_credits': {'artist_list': []},
                'restricted': 0,
                'updated_time': '2022-01-04T14:56:27Z',
                'begin_date_year': '',
                'begin_date': '0000-00-00',
                'end_date_year': '',
                'end_date': '0000-00-00'
              }
            }
          ]
        }
      }
    }
               // new Error(response.statusText)
               console.dir(json['message']['body']['artist_list'])
    return json['message']['body']['artist_list']
  }
}

export {
  getMusicTracksData
}

// clé : 7b5d71a6c33a0d21c7177971cf9372c5